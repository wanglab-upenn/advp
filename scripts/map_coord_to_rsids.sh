INTABLE=${1:-/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3p3.with_added_columns.tsv}
DBSNP=${2:-/mnt/data/datasets/dbSNP/All_20180423.bed.gz}

coordBed="${INTABLE}.coord_for_rsid_lookup.bed"
lookupAll="${coordBed%%.bed}.lookup.bed"
lookupSNV="${coordBed%%.bed}.lookup.SNV.bed"

# NOTE: using modified tabix that outpus region prefix before overlap string
TABIX="/mnt/data/bin/htslib-1.9/tabix.with_input_region"

# find variant records with missing rsIDs, but with Chr and BP information
#   and prepare bed file with variant coordinates for tabix lookup
awk 'BEGIN{ FS="\t";OFS="\t"; }
     { rectype=$10; snp=$25; chr=$27; pos=$29;
			 gdbchr=$55; gdbchrnum=gdbchr; gsub(/^chr/,"",gdbchrnum); gdbpos=$56;
			 if (rectype=="SNP-based" && snp=="NA" && chr!="NR" && pos!="NA" && pos!="NR")
				 print chr,  pos-1, pos # print bed line
		 }' "${INTABLE}" | LC_ALL=c sort -k1,1 -k2,2n -k3,3n > "${coordBed}"

if [ ! -s "${coordBed}" ]; then
	cat "$INTABLE"
else
# use tabix to lookup snp ids by coord
"${TABIX}" "${DBSNP}" -R "${coordBed}" > "${lookupAll}"
# filter out non-SNV records
grep "VC=SNV" "${lookupAll}" > "${lookupSNV}"
# populate snp column with found rsIDs
awk 'BEGIN{FS="\t";OFS="\t"}
 { if (FILENAME==ARGV[1])
	 {
		 region=$1;
		 rsid=$5;
		 rsIDs[region]=(rsid "_MAPPED"); # map region/coord to rsIDs
	 }
   else
	 {   snpCol=25;
			 rectype=$10; snp=$snpCol; chr=$27; pos=$29;
			 gdbchr=$55; gdbchrnum=gdbchr; gsub(/^chr/,"",gdbchrnum); gdbpos=$56;
			 if (rectype=="SNP-based" && snp=="NA" && chr!="NR" && pos!="NA" && pos!="NR")
			 {
				 chrNum=chr; gsub(/^chr/,"",chrNum);
         region=(chrNum ":" pos "-" pos);
			   rsid=rsIDs[region];
			   if (rsid=="") 
				   rsid="NOT_FOUND";
				 $snpCol=rsid; # set snp col to the found rsID
			 }
			 print
	 }
 }' "${lookupSNV}" "${INTABLE}"
fi


