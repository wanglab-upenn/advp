gwasCatalog=${1:-gwas_catalog_v1.02.tsv}
#cut -f 12,13,14,15,21,22,27,28,31,35 
cat ${gwasCatalog} | \
	awk 'BEGIN{FS="\t"; OFS="\t"}
       { pmid=$2;
				 chr=("chr" $12); pos=$13; 
				 reportedGene=$14; mappedGene=$15;
				 strongest=$21; snps=$22;
				 raf=$7; pval=$28; orOrBeta=$31;
				 trait=$35;
				 #if (trait~/Alzheimer/)
				 if (trait~/Alzheimer'"'"'s disease/)
					 print chr, pos, snps, reportedGene, mappedGene, pval, strongest, pmid, trait 
			 }'
