# chr	chrStart	chrEnd	homer_list of tissues	homer_number of tissues	homer_number of overlaps	fantom5_list of tissues	fantom5_number of tissues	fantom5_number of overlaps	GTEx_list of tissues	GTEx_number of tissues	GTEx_number of overlaps	roadmap_list of tissues	roadmap_number of tissues	roadmap_number of overlaps
awk 'BEGIN{FS="\t"; OFS="\t"}{
       if (NR==1)
	   {
		 num_files = ARGC-1
		 num_fields=NF-3; # number of fields - number of columns used for joining
		 
		 # null string
		 NA_string="NA";
		 for (i=2; i<=num_fields; ++i)
			 NA_string = (NA_string "\t" "NA");

		 # this is just to match current Spark metatable output format
		 # the method above is more generic
         NA_string="None	-1	-1"
	   }
	   h=($1 "\t" $2 "\t" $3); # coordinates; fields used for joining
	   $1=""; $2=""; $3="";
	   gsub("^[[:space:]]+", "");
	   line=$0; # original line - fields used for joining
       fi=ARGIND; # index of the file being processed
	   joined[(h"\t"fi)]=line; # store each line along with the file index
	   intervals[h]=1;
     }END{ # output joined metatable
	       for (h in intervals)
		   {
			 printf "%s", h;
			 for (i=1; i<=num_files; ++i)
		     {
			   s=joined[(h"\t"i)];
			   if (!s) {s=NA_string};
			   printf "\t%s", s;
		     };
		     printf "\n";
		   }
	     }' "$@" | sort -k1,1 -k2,2n -k3,3n
