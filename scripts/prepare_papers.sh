# download MEDLINE records for each paper
cat advp_papers.pubmed_ids | while read -r pmid; do bash get_medline_record.sh $pmid > medline/$pmid.medline; echo "$pmid"; done

for f in medline/*.medline; do python3.6 parse_medline.py "${f}"; done >> advp_papers_table.medline.tsv

cut -f1,2,10,11 advp_papers.raw_table.tsv > advp_papers.raw_table.paper_pubmed_tables_reason.tsv

awk 'BEGIN{FS="\t";OFS="\t"}{if (FILENAME==ARGV[1]) {pid=$1; pmid=$2; pids[pmid]=pid; other[pmid]=($3 "\t" $4);}else{pmid=$1; pid=pids[pmid]; if (pid=="") {printf "ERROR: %s not found\n"; exit}; print pid, $0, other[pmid]} }' advp_papers.raw_table.paper_pubmed_tables_reason.tsv advp_papers_table.medline.tsv > advp_papers_table.medline.with_paperidx_and_other.tsv
