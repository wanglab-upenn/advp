
ADVPMETA=${1:-"/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3p7.with_added_columns.tsv"}

awk 'BEGIN{FS="\t";OFS="\t"}
{
	if (NR==1) { print $0, "nonref_allele", "nonref_effect", "nonref_freq", "minor_allele", "minor_effect", "minor_freq", "OR_nonref", "OR_minor"; next };

	# fields for use in normalization
	rsid=$25; ra1=$30; ra2=$31; ranote=$32; raf=$33; effType=$36; eff=$37; gdbref=$57; gdbalt=$58;
  gsub(/^-/,"-", eff);
	# copy reported information into normalized/minor columns 
	norm_allele=ra1; norm_eff=eff; norm_af=raf;
	minor_allele=ra1; minor_af=raf; minor_eff=eff;

	# records to be normalized
	if (rsid~/^rs[0-9]+$/ && ra1~/^[ACGT]$/ && (effType=="OR" || effType~/^[Bb]/))
	{
		# compute effect/AF wrt Non-ref allele
		if (ra1==gdbref)
		{
		  norm_allele=ra2; if (ra2!~/^[ACGT]/) norm_allele=("not_" ra1);
			if (raf~/[0-9.]/) norm_af=1-raf;
		 	if (effType~/[Bb]/) norm_eff = -1.0*eff;
			if (effType=="OR") norm_eff = 1.0/eff;
		};

		# compute effect/AF wrt Minor allele
		if (raf<1.0 && raf>0.5)
		{ minor_allele=ra2; if (ra2!~/ACGT/) minor_allele=("not_" ra1);
			minor_af = 1.0 - raf;
			minor_eff = eff;
			if (effType~/[Bb]/) minor_eff = -1.0*eff;
			if (effType=="OR") minor_eff = 1.0/eff;
		};

	}

	minor_OR="NA"; norm_OR="NA";
  if (effType~/[Bb]/) # reported effect is a beta value
	{
		minor_OR = exp( minor_eff );
		norm_OR = exp( norm_eff );
	}
	if (effType=="OR")
	{
		minor_OR=minor_eff;
		norm_OR=norm_eff;
	}
	print $0, norm_allele, norm_eff, norm_af, minor_allele, minor_eff, minor_af, norm_OR, minor_OR; 
	#print rsid, ra1, ra2, eff, effType, raf, gdbref, gdbalt, norm_allele, norm_eff, norm_af, minor_allele, minor_eff, minor_af;
}' "${ADVPMETA}"

# compute effect/AF w.r.t NON-REF allele
# note1:
#   this normalization will only be perfomed if reported allele == genomic REF;
#     otherwise original allele, effect and freq info will be used
# note2: 
#   if the second allele is not reported, cannot say for sure if it's equal to ALT or not


