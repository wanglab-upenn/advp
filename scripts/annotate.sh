#snpTable=${1:-advp_loci_18sept2019.selected_columns.tsv} # raw table, without additional columns
#gdbIDs=${2:-advp_loci_18sept2019.genomicsDB_IDs}
#snpTable=${1:-"Pavel_P175-P200_pavel_v2p1.txt"} # raw table, without additional columns

#snpTable=${1:-"Pavel_round2_10jun2020.tsv"} # raw table, without additional columns
snpTable=${1:-"Pavel_round2_10jun2020.tsv"} # raw table, without additional columns
snpTable=${1:-/mnt/data/datasets/advp/update_june2020/advp_06july2020.tsv}
snpTable=${1:-/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3p3.txt}
snpTable=${1:-/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3p4.tsv}

#gdbIDs=${2:-"/mnt/data/advp/update_june2020/advp.rsIDs.joined_sept_june.genomicsDB.txt"}
# this is more detailed output from GenomicsDB, including impact scores, gene, etc
gdbIDs=${2:-"/mnt/data/datasets/advp/update_june2020/advp_06july2020.tsv.rsIDs_only.genomicsDB.with_impact"}
gdbIDs=${2:-"/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3.with_added_columns.rsIDs_only.genomicsDB.with_impact.tsv"}
gdbIDs=${3:-"/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3p4.tsv.genomicDB.with_impact.tsv"}

IDtable=${3:-"IDtable.txt"}
IDtable=${3:-"/mnt/data/datasets/advp/update_june2020/hg19_ensembl_id_to_gene_symbol.txt"}
#paperTable=${4:-"advp_papers_17sept2019.tsv.paper_ids"}
#paperTable=${4:-"advp_papers_10june2020.tsv.paper_ids"}
paperTable=${4:-"/mnt/data/datasets/advp/update_june2020/advp_papers_table.medline.with_paperidx_and_other.tsv.paper_ids"}
#closestGeneTable=${5:-"/mnt/data/datasets/advp/update_june2020/advp_06july2020.tsv.rsIDs_only.genomicsDB.closest.bed.rsid_ensid_genename"}
closestGeneTable=${5:-"/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3p4.tsv.genomicDB.with_impact.tsv.closest.bed.rsid_ens_symb_dist"}
closestGeneTable=${5:-"/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3p4.tsv.genomicDB.with_impact.tsv.closest.bed.rsid_ens_symb_dist.unique"}

# get rsIDs for SNP-based record without reported rsIDs
snpTableMappedRsids="${snpTable%.*}.mapped_rsIDs.tsv"
bash map_coord_to_rsids.sh "${snpTable}" > "${snpTableMappedRsids}"
snpTable="${snpTableMappedRsids}"

awk 'BEGIN{FS="\t";OFS="\t"}
    {
       if (FILENAME==ARGV[1])
       {
         # map rsID to genomicsDB ID
         rsID=$3; gdbID=$1;
         rsIDtoGDB[rsID]=gdbID;

         # map rsID to chr, chrStart, ref and alt alleles
         #chr=$6; chrStart=$7;
         chr=$10; chrStart=$11;
         #ref=$4; alt=$5;
         ref=$8; alt=$9;
				 gene=$14;
				 impactedGene=$20;
				 conseq=$19;
				 topEffectImpact=$18;

				 # UPDATE: october 2020
				 # use genomics DB id to derive chr, pos, rsid, etc
				 # FIXME: this seems to also work for deprecated rsids!!??
				 # 11:121361015:G:A_rs2298525
				 split($1,a,":");
				 chr=("chr" a[1]);
				 chrStart=a[2];
         chrEnd=a[2];
         rsID=a[4];
				 gsub(/^.+_rs/,"rs",rsID);

         rsIDtoChr[rsID]=chr;
         rsIDtoChrStart[rsID]=chrStart;
         rsIDtoRef[rsID]=ref;
         rsIDtoAlt[rsID]=alt;
				 rsIDtoGene[rsID]=gene;
         rsIDtoImpactedGene[rsID]=impactedGene;
				 rsIDtoConsequence[rsID]=conseq;
				 rsIDtoTopEffectImpact[rsID]=topEffectImpact;
       }
       else if (FILENAME==ARGV[2])
       {
         # map gene symbol to Ensembl ID (used to connect with Genomics DB)
         ensemblID=$4;
         geneSymbol=$1; 
         ensemblID=$2;
         geneSymbol=$3; 
         ensemblIDs[geneSymbol]=ensemblID;
       }
       else if (FILENAME==ARGV[3])
       {
         # map paperIDX to pubmed IDs
         if (FNR==1) next; # skip header
         paperID=$1; 
         pubmedID=$2;
         pubmedIDs[paperID]=pubmedID;
       }
		   else if (FILENAME==ARGV[4])
			 {
				 # add nearest gene information
				 rsId=$1; ens=$2; gene=$3; dist=$4;
				 # remove ens id version
				 gsub(/.[0-9]+$/,"",ens);
				 if (!nearestGeneEns[rsId])
				 {
           nearestGeneEns[rsId]=ens;
				   nearestGeneSymb[rsId]=gene;
				   nearestGeneDist[rsId]=dist;
				 }
			   else
				 {
					 #nearestGeneEns[rsId]=(nearestGeneEns[rsId] ";" ens);
					 #nearestGeneSymb[rsId]=(nearestGeneSymb[rsId] ";" gene);
					 #nearestGeneDist[rsId]=(nearestGeneDist[rsId] ";" dist);
					 nearestGeneEns[rsId]=(nearestGeneEns[rsId] "_" ens);
					 nearestGeneSymb[rsId]=(nearestGeneSymb[rsId] ";" gene);
					 nearestGeneDist[rsId]=(nearestGeneDist[rsId] ";" dist);
				 }
			 }
       else
       {
         if (FNR==1)
         {
           gdbIDColName="Genomics DB ID"
           ensemblIDColName="Ensembl ID"
           pubmedIDColName="Pubmed ID"
           gdbChrColName="gdb_chr";
           gdbChrStartColName="gdb_chrStart";
           gdbRefColName="gdb_REF";
           gdbAltColName="gdb_ALT";
					 gdbGeneColName="gdb_gene";
					 gdbImpactedGeneColName="gdb_impacted_gene";
					 closestEnsColName="nearest_gene_ens";
					 closestSymbColName="nearest_gene_symb";
					 closestDistColName="nearest_gene_distance";
					 locusNameEnsColName="locusname_ens";
					 conseqColName="most_severe_consequence";
           #print $0, gdbChrColName, gdbChrStartColName, gdbRefColName, gdbAltColName, gdbIDColName, ensemblIDColName, pubmedIDColName; 
           print $0, gdbChrColName, gdbChrStartColName, gdbRefColName, gdbAltColName, gdbIDColName, ensemblIDColName, pubmedIDColName, gdbGeneColName, gdbImpactedGeneColName, closestEnsColName, closestSymbColName, closestDistColName, locusNameEnsColName, conseqColName; 
           next;
         }
         rsID=$25;
         gsub(/[[:space:]]+/,"",rsID);
         geneSymbol=$24; # reported gene symbol
				 locusNameSymbol=$52; # reported locus/gene symbol
         paperID=$3; # internal paper id
         #chrIdx = 20; chrStartIdx=21; refIdx=23; altIdx=24; 

         #lookup in GenomicsDB
         gdbID=rsIDtoGDB[rsID];
         if (gdbID=="")
           gdbID="NA";

         ensemblID=ensemblIDs[geneSymbol];
         if (ensemblID=="")
           ensemblID="NA";
         
				 locusNameEnsemblID=ensemblIDs[locusNameSymbol];
         if (locusNameEnsemblID=="")
           locusNameEnsemblID="NA";

         pubmedID=pubmedIDs[paperID]
         if (pubmedID=="")
           pubmedID="NA"
         
         # fill in chr, position, ref, alt positions
         gdbChr=rsIDtoChr[rsID]
         gdbChrStart=rsIDtoChrStart[rsID];
         gdbRef=rsIDtoRef[rsID];
         gdbAlt=rsIDtoAlt[rsID];
				 gdbGene=rsIDtoGene[rsID];
				 gdbImpactedGene=rsIDtoImpactedGene[rsID];
				 closestGeneEns=nearestGeneEns[rsID];
				 closestGeneSymb=nearestGeneSymb[rsID];
				 closestDist=nearestGeneDist[rsID];
         conseq=rsIDtoConsequence[rsID];
				 if (conseq == "") conseq="NA"; 

         #if ($chrIdx != "" && $chrIdx!="NA"  && ("chr" $chrIdx) != gdbChr)
         #   printf "ERROR: line %d: rsID=%s : %s != %s\n", FNR, rsID, $chrIdx, gdbChr;
         #if ($altIdx != "" && $altIdx != gdbAlt)
         #    printf "ERROR: line %d: rsID=%s : %s != %s\n", FNR, rsID, $altIdx, gdbAlt;

         if (0==1) {
         if (gdbChr!="")
           $chrIdx = gdbChr 
         $chrStartIdx = rsIDtoChrStart[rsID];

         # only fill in alleles if they are missing
         if ($refIdx=="")
           $refIdx=gdbRef
         fi ($altIdx=="")
           $altIdx = gdbAlt
         }
         #print $0, gdbChr, gdbChrStart, gdbRef, gdbAlt, gdbID, ensemblID, pubmedID; 
				 # UPDATE: october 2020
				 # remove unnecessary quotes
				 n=split("11,12,17",cols,",") # cohort, imputation
				 for (i=1; i<=n; ++i)
					 gsub(/"/,"",$(cols[i]));

         print $0, gdbChr, gdbChrStart, gdbRef, gdbAlt, gdbID, ensemblID, pubmedID, gdbGene, gdbImpactedGene, closestGeneEns, closestGeneSymb, closestDist, locusNameEnsemblID, conseq; 
       }
    }' "${gdbIDs}" "${IDtable}" "${paperTable}" "${closestGeneTable}" "${snpTable}" 




