shopt -s expand_aliases
source ~/.bashrc

INADVP=${1:-/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3p5.with_added_columns.tsv}
OUTPREFIX=${2:-"${INADVP}.stats"}
# snp reported in >1 paper
awk 'BEGIN{ FS="\t"; OFS="\t"; }
     {
			rsid=$25; pid=$3;
			h=(rsid "\t" pid);
			if (!snpPaper[h])
			{
				snpPaper[h]=1; # new paper, rsid combination
				uniqPaperCnt[rsid]++; # count unique papers for snp
				if (!snpPapers[rsid])
					snpPapers[rsid]=pid;
				else snpPapers[rsid] = (snpPapers[rsid] ";" pid); # list of papers
			}
		 }END{ for (rsid in uniqPaperCnt)
		       {
						 if (uniqPaperCnt[rsid]>1)  print rsid, uniqPaperCnt[rsid], snpPapers[rsid];
					 }
		 }' "${INADVP}" > ${OUTPREFIX}.variants_multiple_papers 
rsidCol=25
locusCol=52
cohortCol=12
studyCol=8
paperIdCol=3
tableIdCol=4
conseqCol=68
cohortSimpleCol=51
numVariants=$(cut -f ${rsidCol}  "${INADVP}" | tail -n+2 | grep -v "^N" | sort -u | wc -l)
# count only rsIDs:
numVariants=$(cut -f ${rsidCol} "${INADVP}" | tail -n+2 | grep "^rs" | sort -u | wc -l)
numLoci=$( cut -f ${locusCol} "${INADVP}" | sort -u | wc -l)
numTables=$(cut -f $paperIdCol,$tableIdCol "${INADVP}" | sort -u | wc -l)
numRecords=$( tail -n+2 "${INADVP}" | wc -l | awk '{print $1}')
echo "Number of records=$numRecords"
echo "Number of unique genetic variants=$numVariants"
echo "Number of unique reported loci=$numLoci"
echo "Number of curated tables=$numTables"
# cohorts
cut -f ${cohortCol} "${INADVP}" | sort -u | sed 's/"//g' | awk '{n=split($0,a,", "); for (i=1;i<=n;++i) print a[i]}' | sort -u > ${OUTPREFIX}.cohorts
# study type 
cut -f $studyCol "${INADVP}" | tail -n+2 | freq > ${OUTPREFIX}.record_types
cut -f 7,$studyCol "${INADVP}" | tail -n+2 | freq > ${OUTPREFIX}.record_types_and_stage
# consequences
cut -f ${conseqCol} "${INADVP}" | tail -n+2  | freq > ${OUTPREFIX}.most_severe_conseq
#grep -a -E "SNP-based" "${INADVP}" | cut -f 68 | freq > ${OUTPREFIX}.most_severe_conseq.SNP_based_records_only
#grep -a -E "SNP-based" "${INADVP}" | cut -f 68 | freq > ${OUTPREFIX}.most_severe_conseq.SNP_based_records_only
# cohorts
cut -f ${cohortSimpleCol} "${INADVP}" | tail -n+2 | freq > ${OUTPREFIX}.cohorts_simple3
cut -f ${cohortSimpleCol} "${INADVP}" | tail -n+2 | sed 's/"//g' | awk '{n=split($0,a,", "); for (i=1;i<=n;++i) print a[i]}' | freq > ${OUTPREFIX}.cohorts_simple3.split

