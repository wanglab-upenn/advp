# compare GenomicDB gene(s) and Gencode nearest gene(s)
# awk 'BEGIN{FS="\t";OFS="\t"}{rsid=$25; gdbgene=$(NF-4); nearestgene=$(NF-1); no_match=1; ngdb=split(gdbgene,gdbgenes,", "); n=split(nearestgene,nearestgenes,";"); for (i=1; i<=ngdb && no_match;++i) {for (j=1; j<=n; ++j) if (gdbgenes[i]==nearestgenes[j]) {no_match=0; break}}; if (no_match==1 && gdbgene!="null" && n>0 && ngdb>0) print gdbgene,nearestgene, rsid}' advp_06july2020.with_added_columns.tsv | sort -u | wc -l

awk 'BEGIN{FS="\t"; OFS="\t"}
     {
			 if (FILENAME==ARGV[1])
			 {
				  rsid=$1; gene=$3;
					nearestGenes[rsid]=gene;
			 }
		   else
			 {
				 locusname=$2; rsid=$1;
				 nearestgene=nearestGenes[rsid];
				 if (nearestgene=="") nearestgene="NOT_FOUND";
				 print rsid, locusname, nearestgene
			 }
		}' advp_06july2020.tsv.rsIDs_only.genomicsDB.closest.bed.rsid_ensid_genename advp_loci_18sept2019.selected_columns.tsv.rsid_locusname
