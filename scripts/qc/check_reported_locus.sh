
#awk 'BEGIN{FS="\t";OFS="\t"}{attr=$10; geneName=attr; gsub(/^.*gene_name=/,"",geneName); gsub(/;.+$/,"",geneName); chr=$1; chrStart=$2+1; chrEnd=$3; print geneName, chr, chrStart, chrEnd}' gencode.v34lift37.annotation.protein_coding_genes_only.bed | sort -k1,1 -u -s > gencode.v34lift37.annotation.protein_coding_genes_only.gene_coord_table
GENECOORD=gencode.v34lift37.annotation.protein_coding_genes_only.gene_coord_table

ADVPTABLE=${1:-/mnt/data/datasets/advp/update_june2020/ADVP_0706_v3p5.with_added_columns.duplicated_gene_fix.multi_ethnic_fix.with_allele_norm.tsv}

reportedLocusCol=52 # LocusName

# extract all unique locus names
locusNames="${ADVPTABLE}.locus_names"
cut -f ${reportedLocusCol}  "${ADVPTABLE}" | tail -n+2 | sort -u > "${locusNames}"

awk 'BEGIN{FS="\t";OFS="\t"; repLocusCol="'${reportedLocusCol}'"+0;}
{
  if (ARGIND==1) 
	{
		# gene coord lookup table
		gene=$1; geneCoord[gene]=( $2 "\t" $3 "\t" $4 ); # chr chrStart1 chrEnd1
	}
  else if (ARGIND==2)
	{
		# ADVP records table
		if (FNR==1)
		{
			print $0, "reported_locus_chr", "reported_locus_chrStart", "reported_locus_chrEnd";
		  next;
		}
		locus=$repLocusCol;
    # lookup genomic coordinates for the reported locus
		gsub(/[[:space:]]/,"",locus);
    locusCoord=geneCoord[locus];
		if (locusCoord!~/^chr/)
			locusCoord=("NOT_FOUND" "\t" "NOT_FOUND" "\t" "NOT_FOUND");
	
		# check whether genomic coordinates match	between reported locus and the snp 
		n=split(locusCoord,a,"\t");
		locusChr=a[1];
    locusChrStart=a[2];
		locusChrEnd=a[3];
		# snp coordinates
		gdbChr=$55;
		gdbChrStart=$56;
		nearest_gene=$65;
		distToLocus = "NA";
		if (gdbChr==locusChr && locusChr~/chr/)
		{
			if (gdbChrStart<=locusChrEnd && gdbChrStart>=locusChrStart)
				distToLocus=0; # variant is within gene body
			else if (gdbChrStart<locusChrStart)
				distToLocus=gdbChrStart-locusChrStart; # upstream variant
			else if (gdbChrStart>locusChrEnd)
				distToLocus=gdbChrStart-locusChrEnd; # downstream variant
		}
		distanceThres=50000;
		if (distToLocus!="NA" && distToLocus>distanceThres && locus!=nearest_gene)
			print $0, locus, locusCoord, gdbChr, gdbChrStart, nearest_gene, distToLocus  > "'${ADVPTABLE}'.position_mismatch"
		if (gdbChr!=locusChr && locusChr~/chr/ && gdbChr~/chr/)
			print $0, locusCoord > "'${ADVPTABLE}'.chr_mismatch"
		print $0, locusCoord;
	}
}' "${GENECOORD}" "${ADVPTABLE}"




