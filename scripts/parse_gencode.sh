
awk 'BEGIN{FS="\t"}{t=$3; if (t=="gene" && $9~/gene_type=lncRNA/) print }' gencode.v34lift37.annotation.gff3 > gencode.v34lift37.annotation.lncRNA_only.gff3
gff2bed < gencode.v34lift37.annotation.lncRNA_only.gff3 > gencode.v34lift37.annotation.lncRNA_only.bed
awk 'BEGIN{FS="\t";OFS="\t"}{attr=$10; genename=attr; gsub(/^.+gene_name=/,"",genename); gsub(/;.+$/,"",genename); print $0, genename }' gencode.v34lift37.annotation.lncRNA_only.bed > gencode.v34lift37.annotation.lncRNA_only.with_gene_symbol.bed
cut -f11 gencode.v34lift37.annotation.lncRNA_only.with_gene_symbol.bed | sort -u > gencode.v34lift37.annotation.lncRNA_gene_names.txt
