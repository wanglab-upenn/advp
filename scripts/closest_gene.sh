# FINE closest upstream, downstream genes
set -eu
INBED=${1:-advp_06july2020.tsv.rsIDs_only.genomicsDB.bed}
GENEBED=${2:-gencode.v34lift37.annotation.protein_coding_genes_only.bed}
BEDTOOLS="/usr/bin/bedtools"
BEDTOOLS="/mnt/data/bin/bedtools-2.29.2/bin/bedtools"

#"${BEDTOOLS}" closest -sorted -D ref -a advp_06july2020.tsv.rsIDs_only.genomicsDB.bed -b gencode.v34lift37.annotation.protein_coding_genes_only.bed > advp_06july2020.tsv.rsIDs_only.genomicsDB.closest.bed
#awk 'BEGIN{FS="\t"; OFS="\t"}{rsid=$4; ens=$8; attr=$14; dist=$NF; genename=attr; gsub(/^.+gene_name=/,"",genename); gsub(/;.+$/,"",genename); print rsid, ens, genename, dist }' advp_06july2020.tsv.rsIDs_only.genomicsDB.closest.bed | sort -u > advp_06july2020.tsv.rsIDs_only.genomicsDB.closest.bed.rsid_ensid_genename
CLOSESTBED="${INBED%.bed}.closest.bed"
OVERLAPPINGBED="${INBED%.bed}.overlapping.bed"
CLOSESTUPBED="${INBED%.bed}.closest.up.bed"
CLOSESTDOWNBED="${INBED%.bed}.closest.down.bed"
CLOSESTSUMMARY="${CLOSESTBED}.summary"


"${BEDTOOLS}" closest -sorted -D ref -a "${INBED}" -b "${GENEBED}" > "${CLOSESTBED}" # closest: upstream (dist < 0), overlapping (dist=0), downstream (dist>0)
awk '{dist=$NF; if (dist==0) print}' "${CLOSESTBED}" > "${OVERLAPPINGBED}" # stricltly overlapping (dist = 0)
"${BEDTOOLS}" closest -sorted -D ref -id -io -a "${INBED}" -b "${GENEBED}" > "${CLOSESTUPBED}" # stricltly upstream (dist < 0)
"${BEDTOOLS}" closest -sorted -D ref -iu -io -a "${INBED}" -b "${GENEBED}" > "${CLOSESTDOWNBED}" # strictly downstream (dist > 0)
# NOTE1: distances are measured wrt B (B_pos-A_pos)
# NOTE2: strictly upstream or downstream genes may not exist (e.g., for positions close to chromosome ends)
#        in this case, bedtools reports . -1 -1 for chr, start, end and -1 for the distance
#awk 'BEGIN{FS="\t"; OFS="\t"}
#     {
#			 rsid=$4; ens=$8; attr=$14; dist=$NF;
#			 genename=attr;
#			 gsub(/^.+gene_name=/,"",genename); gsub(/;.+$/,"",genename);
#			 print rsid, ens, genename, dist
#		 }' "${CLOSESTBED}" | sort -u > "${CLOSESTSUMMARY}"
awk 'BEGIN{FS="\t"; OFS="\t"}
     {
			 rsid=$4; ens=$8; attr=$14; dist=$NF;
			 genename=attr;
			 gsub(/^.+gene_name=/,"",genename); gsub(/;.+$/,"",genename);
			 fileIdx=ARGIND;
			 h=(rsid "\t" fileIdx);
			 if (length(genes[h])==0)
			   genes[h]=genename;
			 else
				 genes[h]=(genes[h] "; " genename);
       dists[h]=dist;
       rsIDs[rsid]=1;
			 #if (FILENAME==ARGV[1])
			 #{
			 #	 if (length(closestGene[rsID])==0)
		   #		  closestGene[rsID]=genename;
			 #	 else
			 #		  closestGene[rsID]=(closestGene[rsID] "; " genename);
			 #	 closestDist[rsID]=dist;
			 #}
			 #print "read", ARGIND, h, rsid, ens, genename, dist
		 }
		 END{	# print header
		      print "#rsid\tclosest\tclosest_dist\toverlapping\toverlapping_dist\tupstream\tupstream_dist\tdownstream\tdownstream_dist\tmapped\tmapped_dist"
		      # print snp, closest, overlapping, upstream, downstream genes and distances
		      for (rsid in rsIDs)
					{
						printf "%s\t", rsid;
						h=(rsid "\t" 1);
		        gene=genes[h]; dist=dists[h];
					  if (gene=="" || gene==".")
						{
							  # this should not happen for major chromosomes
							  print "ERROR: closest gene not found!!!!" > /dev/stderr
								gene="NOT_FOUND";
								dist="NA";
						}
		        printf "%s\t%s", gene, dist;
		        for (i=2; i<ARGC; ++i)
					  {	
						  h=(rsid "\t" i);
		          gene=genes[h]; dist=dists[h];
							if (gene=="" || gene==".") # handle not found genes
							{
								gene="NOT_FOUND";
								dist="NA";
							}
		          printf "\t%s\t%s", gene, dist; 
					  }
						overlappingGene=genes[(rsid "\t" 2)];
						upstreamGene=genes[(rsid "\t" 3)];
						downstreamGene=genes[(rsid "\t" 4)];
						mappedDist=dists[(rsid "\t" 1)]; # use closest distance
            if (overlappingGene=="")
							mappedGene=(upstreamGene " --- " downstreamGene);
						else
							mappedGene=overlappingGene;
						printf "\t%s\t%s", mappedGene, mappedDist;
						printf "\n";
		      }
				}' "${CLOSESTBED}" "${OVERLAPPINGBED}" "${CLOSESTUPBED}" "${CLOSESTDOWNBED}" > "${CLOSESTSUMMARY}"

