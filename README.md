# README #

This repository contains [ADVP](https://advp.niagads.org) data processing and analysis scripts.

### Citation ###
Kuksa PP, Liu C-L, Fu W, Qu L, Zhao Y, Katanic Z, Kuzma A, Ho P.-C., Tzeng K.-T., Valladares O, Chou S.-Y., Naj AC, Schellenberg GD, Wang L.-S., Leung YY. *Alzheimer’s disease variant portal (ADVP): a catalog of genetic findings for Alzheimer’s disease*. MedRxiv 2020:2020.09.29.20203950. [https://doi.org/10.1101/2020.09.29.20203950](https://doi.org/10.1101/2020.09.29.20203950)



